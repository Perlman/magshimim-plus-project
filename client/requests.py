import socket
import threading
import pathlib
import base64
import Blocker
# from AdBlock import *

PORT = 5548
SERVER_ADDR = "127.0.0.1"

ADMIN_PASS = b'QWRtaW4='

FAIL_MARK = " :("
SUCCESS_MARK = " :)"
STATUS = 0
DATA = 1
SEPERATOR = '*'
FILE_NAME = str(pathlib.Path(__file__).parent.absolute()) + "/blacklist.txt"


def get_block_list():
    try:
        f = open(FILE_NAME, "r")
    except FileNotFoundError:
        f = open(FILE_NAME, "w")
        f.close()
        return []
    block_list = f.read().split('\n')
    f.close()
    return [x for x in block_list if x != '']


def set_block_list(blist):
    f = open(FILE_NAME, "w")
    f.write(blist)
    f.close()


def connect():
    connection_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    while True:
        try:
            connection_socket.connect((SERVER_ADDR, PORT))
            break
        except:
            pass
    return connection_socket


def talk_to_server(req):
    connection_socket = connect()
    connection_socket.sendall(req.encode())
    res = connection_socket.recv(1024).decode().split(SEPERATOR)
    connection_socket.close()
    return res


def sign_in(username, password):
    log_in_req = "signin %s %s" % (username, password)
    res = talk_to_server(log_in_req)
    if FAIL_MARK in res[STATUS]:
        print(res[STATUS])
    return SUCCESS_MARK in res[STATUS]


def get_user_credits():
    username = input("Enter username: ")
    password = input("Enter password: ")
    return (username, password)


# def push():
#     username, password = get_user_credits()
#     success = sign_in(username, password)
#     if success:
#         blist = get_block_list()
#         blist = "\n".join(blist)
#         push_req = "push %s %s" % (username, blist)
#         res = talk_to_server(push_req)
#         print(res[STATUS])


# def pull():
#     username, password = get_user_credits()
#     success = sign_in(username, password)
#     if success:
#         push_req = "pull %s" % (username)
#         res = talk_to_server(push_req)
#         set_block_list(res[DATA])
#         print(res[STATUS])


# def register():
#     username, password = get_user_credits()
#     register_req = "register %s %s" % (username, password)
#     res = talk_to_server(register_req)
#     print(res[STATUS])


def admin_authenticate():
    admin_pass = input("Enter admin pass: ")
    if base64.b64encode(admin_pass.encode()) == ADMIN_PASS:
        return True
    return False


def handle_req(req):
    req_type = req.split(' ')[0]
    if req_type not in requests_dict.keys():
        print("unrecognize command:", req_type)
        return
    if not admin_authenticate():
        print("Password is incorrect! ", req_type)
        return
    requests_dict[req_type]()


def update(username="", password=""):
    # if username is "" or password is "":
    #     username, password = get_user_credits()
    success = sign_in(username, password) or True
    if success:
        req = "update"
        res = talk_to_server(req)
        # blist = get_block_list()
        # res[DATA] = list(dict.fromkeys(blist + res[DATA].split('\n')))
        Blocker.init_blocking(res[DATA].split('\n'))
        set_block_list('\n'.join(res[DATA]))
        print(res[STATUS])


def print_sites():
    print("sites:")
    for site in get_block_list():
        print("-> " + site)


requests_dict = {
    "print" : print_sites,
    # "push" : push,
    # "pull" : pull,
    # "register" : register,
    "update" : update
}
