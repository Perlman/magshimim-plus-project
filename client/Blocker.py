from requests import *
import iptc
from socket import *
import re

_rules = []
username = "ehudp"
password = "322462714"

def get_domain_ips(domain):
    addresses = []
    if "www" not in domain and "http" not in domain:
        domain = "www." + domain
    try:
        addresses.append(gethostbyname_ex(domain)) 
    except:
        print("invalid domain -", domain)
        return []
    return addresses[-1][-1]


def block_ip(ip):
    rule = iptc.Rule()
    rule.src = ip
    target = iptc.Target(rule, "DROP")
    rule.target = target
    chain = iptc.Chain(iptc.Table(iptc.Table.FILTER), "INPUT")
    chain.insert_rule(rule)
    _rules.append(rule)


def block_domain(domain):
    ips = get_domain_ips(domain)
    for ip in ips:
        block_ip(ip)


def contains_ip(string):
    return re.match(r"^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$", string) is not None


def delete_rules():
    for table in [iptc.Table(t) for t in iptc.Table.ALL]:
        for chain in table.chains:
            if chain.rules:
                for rule in _rules:
                    try:
                        chain.delete_rule(rule)
                    except:
                        pass


def block_sites(sites=None):
    if sites is None:
        sites = get_block_list()
    for site in sites:
        if site is not '':
            if contains_ip(site):
                block_ip(site)
            else:
                block_domain(site)


def init_blocking(blist=None):
    delete_rules()
    block_sites(blist)


def main():
    block_sites()
    print("Blocker is on")

    try:
        while True:
            req = input("BlockerCommandLine:~$ ")
            if req.lower() == "exit" and admin_authenticate():
                raise Exception('', '')
            handle_req(req)
    except:
        delete_rules()
        print("\nBlocker is off")
        exit()

if __name__== "__main__":
    main()
