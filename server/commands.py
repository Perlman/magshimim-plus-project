from database import *

FAIL_MARK = " :("
SUCCESS_MARK = " :)"
SEPERATOR = '*'

def pull(params):
    username = params[0]
    try:
        blist = get_list(username)
    except:
        return "failed to pull" + FAIL_MARK
    return "pulled successfuly" + SUCCESS_MARK + SEPERATOR + blist


def push(params):
    username = params[0]
    blist = params[1]
    try:
        push_list(username, blist)
    except:
        return "failed to push" + FAIL_MARK 
    return "pushed successfuly" + SUCCESS_MARK


def register(params):
    if len(params) < 2 or len(params) > 2:
        return "incorrect amount of parameters" + FAIL_MARK
    
    username = params[0]
    password = params[1]
    
    if not user_exist(username):
        insetr_user(username, password)
        return "registered successfuly" + SUCCESS_MARK
    else:
        return "username already exist" + FAIL_MARK


def sign_in(params):
    if len(params) < 2 or len(params) > 2:
        return "incorrect amount of parameters" + FAIL_MARK

    username = params[0]
    password = params[1]
    if authenticate(username, password):
        return "logged in successfuly" + SUCCESS_MARK
    else:
        return "failed to log in" + FAIL_MARK


def update(params):
    all_sites = ""
    try:
        all_sites = get_all_lists()
    except:
        return "failed to update in" + FAIL_MARK
    return "updated successfuly" + SUCCESS_MARK + SEPERATOR + all_sites


commands_dict = {
    "pull" : pull,
    "push" : push,
    "register" : register,
    "signin" : sign_in,
    "update" : update
}