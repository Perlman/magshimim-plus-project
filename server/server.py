import socket
import threading
from commands import *

PORT = 5548
SELF_ADDR = "127.0.0.1"

def client_thread(client_soket):
    command = client_soket.recv(1024).decode().split(' ')
    print("got a command:", command[0])
    client_soket.sendall(commands_dict[command[0]](command[1:]).encode())


def main():
    serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    serversocket.bind((SELF_ADDR, PORT))
    serversocket.listen(5)

    try:
        print("server on")
        while True:
            (client_soket, address) = serversocket.accept()
            threading.Thread(target=client_thread, args=((client_soket, ))).start()
    except KeyboardInterrupt:
        print("\nserver off")
        exit()
    except Exception as e:
        print("error:")
        print(e)

if __name__== "__main__":
    main()