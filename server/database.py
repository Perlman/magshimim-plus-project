import sqlite3
DATABASE_NAME = "usersRecords.db"
IS_DATABASE_EXIST = False

def get_connection():
    connection = sqlite3.connect(DATABASE_NAME)
    return connection


def database_exists():
    sql_command = "SELECT name FROM sqlite_master WHERE type='table' AND name='records';"
    connection = get_connection()
    crsr = connection.cursor()
    crsr.execute(sql_command)
    return len(crsr.fetchall()) == 1


def exec(sql_command):
    if not database_exists():
        init_database()

    res = []
    connection = get_connection()
    crsr = connection.cursor()
    crsr.execute(sql_command) 
    connection.commit()

    if "select" in sql_command.lower():
        res = crsr.fetchall()
    connection.close()
    return res


def init_database():
    sql_command = "CREATE TABLE records (user_name varchar(255) PRIMARY KEY, password varchar(255), list varchar(30000));"
    connection = get_connection()
    crsr = connection.cursor()
    crsr.execute(sql_command) 
    connection.commit()
    connection.close()

def user_exist(user):
    sql_command = "select * from records where user_name='%s';"%(user)
    results = exec(sql_command)
    return len(results) >= 1


def authenticate(user, password):
    sql_command = "select * from records where user_name='%s' and password='%s';"%(user, password)
    results = exec(sql_command)
    return len(results) >= 1


def insetr_user(user, password, blist=""):
    sql_command = "INSERT INTO records VALUES ('%s', '%s', '%s');" % (user, password, blist)    
    exec(sql_command)


def push_list(user, blist):
    sql_command = "UPDATE records SET list = '%s' WHERE user_name = '%s';" % (blist, user)
    exec(sql_command)


def get_list(user):
    sql_command = "select list from records where user_name='%s';"%(user)
    return exec(sql_command)[0][0]


def get_all_lists():
    sql_command = "select list from records;"
    res = exec(sql_command)
    blist = []
    for a in res:
        for b in a:
            for site in b.split('\n'):
                blist.append(site)

    return '\n'.join(list(dict.fromkeys(blist)))
