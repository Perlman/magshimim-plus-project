FROM python:3.7

RUN apt-get update -y

WORKDIR /server

RUN pip install -r requirements.txt

COPY server/server.py
COPY server/database.py
COPY server/commands.py
COPY usersRecords.db

CMD ["python3", "server.py"]
